#!/bin/bash
export MW_INSTALL_PATH=/data/project/slf/core

set -eux

cd /data/project/slf/SecureLinkFixer
git fetch origin
git checkout origin/master
/data/project/slf/www/rust/target/release/slf-bot
#php maintenance/fetchList.php
git commit domains.php -m "Updating domains.php from Mozilla"
git push origin HEAD:refs/for/master%t=domains-update
